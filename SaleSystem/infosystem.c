/**
 * 销售系统各大功能相关函数。
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "infosystem.h"
#include "linklist.h"
#include "brand.h"

extern void infosystem_dispatch(infosystem *isptr, int cmd, char **argv);

const char *g_command_str[] = {
    "help",
    "list_item",
    "load_item",
    "save_item",
    "list_brand",
    "load_brand",
    "save_brand",
    "add_brand",
    "query",
    "sell",
    "purchase",
    "sort_brand",
    "sort_sales",
    "sum_total"
};

const char *g_command_help[] = {
    "Show this help text.",
    "List all items",
    "Load items from file",
    "Save items to file",
    "List brands of an item.",
    "Load brands of an item from file.",
    "Save brands of an item to file.",
    "Add a brand using command line.",
    "Perform a query. E.g. query Refrigerant:R600a",
    "Sell a product. E.g. sell Philips_GC1480/02",
    "Purchase products. E.g. purchase Philips_GC1480/02 200",
    "Sort brands of an item according to specific property. E.g. sort Price_CNY",
    "Sort all brands according to sales.",
    "Sum the total of sales of specific item."
};

/*
 * 各命令的参数个数
 */
const int g_argument_size[] = {
    0, // help
    0, // list_item
    1, // load_item
    1,  // save_item
    1, // list_brand
    2, // load_brand
    2, // save_brand
    1,  // add_brand
    1, // query
    1,  // sell
    2, // purchase
    2, // sort_brand
    0,  // sort_sales
    1   // sum_sales
};

/*
 * 将条目继承树的所有结点组织成一个链表
 * 1. 将根结点添加到链表中
 * 2. 对于每个结点，对以该结点为根的子树执行同样过程
 */
static inline void item_tree_to_linklist(item *root, linklist *list_ptr)
{
    linklist_add(list_ptr, root);
    for (size_t i = 0; i < root->len; i++)
        item_tree_to_linklist(root->children[i], list_ptr);
}

// 函数回调
void __print_item(void *ptr)
{
    item_output(ptr, stdout);
}

// 函数回调，将条目ptr的品牌信息保存到默认文件中
void __save_item_brand(void *ptr)
{
    item *iptr = (item *)ptr;
    
    if (linklist_isempty(iptr->brand_list))
        return;
    char buffer[1025];
    sprintf(buffer, "config/brand_%s.txt", iptr->name);
    FILE *fp = fopen(buffer, "w");
    if (fp)
    {
        brand_save_list(iptr->brand_list, fp);
        fclose(fp);
    }
}

// 函数回调，从默认位置加载条目ptr的品牌信息
void __load_item_brand(void *ptr)
{
    item *iptr = (item *)ptr;
    char buffer[1025];
    sprintf(buffer, "config/brand_%s.txt", iptr->name);
    FILE *fp = fopen(buffer, "r");
    if (fp)
    {
        brand_load_list(iptr, iptr->brand_list, fp);
        fclose(fp);
    }
}

// 回收条目的品牌（使用链表存储）战胜的内存空间
void destroy_item_brand(void *ptr)
{
    item *iptr = (item *)ptr;
    linklist_destroy(iptr->brand_list);
}

// 加载销量与库存信息
// 1. 加载到infosystem对象isptr的相关数组
// 2. 加载到各个item的brand对象的属性中去。
void infosystem_load_sales_stock(infosystem *isptr)
{
    FILE *fp = fopen("config/sales.txt", "r");
    if (!fp)
        return;
    
    char *line = NULL;
    size_t len = 0;
    ssize_t read = 0;
    char buf_key[MAX_STRING_SIZE + 1];
    int sales, stock;
    while ((read = getline(&line, &len, fp)) != -1)
    {
        if (strncmp(line, "\n", 1) == 0)
            continue;
        int result = sscanf(line, "%128s %d %d", buf_key, &sales, &stock);
        assert(result == 3);
        strncpy(isptr->brand_names[isptr->brand_len], buf_key, MAX_STRING_SIZE);
        isptr->brand_sales[isptr->brand_len] = sales;
        isptr->brand_stock[isptr->brand_len] = stock;
        isptr->brand_len++;
    }
    
    fclose(fp);
    
    for (linknode *ptr = isptr->item_list->head; ptr != isptr->item_list->tail; ptr = ptr->next)
    {
        item *pi = (item *)ptr->data_ptr;
        for (linknode *ptr2 = pi->brand_list->head; ptr2 != pi->brand_list->tail; ptr2 = ptr2->next)
        {
            brand *pb = (brand *)ptr2->data_ptr;
            for (size_t i = 0; i < isptr->brand_len; i++)
            {
                if (strncmp(isptr->brand_names[i], pb->name, MAX_STRING_SIZE) == 0)
                {
                    pb->sales = isptr->brand_sales[i];
                    pb->stock = isptr->brand_stock[i];
                    break;
                }
            }
        }
    }
}

/*
 * 初始化infosystem对象
 * 1. 从默认文件读取条目信息并以树的形式组织放入内存中
 * 2. 为访问方便，再把所有的条目信息同时也组织成一个链表，并打印到终端
 * 3. 依次访问链表结点条目，把每个条目的默认品牌信息加载到条目中
 * 4. 从默认的sales.txt加载销量及库存数据
 */
void infosystem_init(infosystem *isptr)
{
    // Step 1
    FILE *fp = fopen("config/item.txt", "r");
    if (fp)
    {
        isptr->base_item = item_load_tree(fp);
        fclose(fp);
        if (isptr->base_item == NULL)
            exit(-2);
    }
    else
    {
        puts("Error initialization: cannot open config/item.txt!");
        exit(-1);
    }
    
    // Step 2
    isptr->item_list = linklist_create();
    item_tree_to_linklist(isptr->base_item, isptr->item_list);
    
    linklist_iterate(isptr->item_list, __print_item);
    
    // Step 3
    linklist_iterate(isptr->item_list, __load_item_brand);
    
    // Step 4
    isptr->brand_len = 0;
    infosystem_load_sales_stock(isptr);
}

const char **__item_name;
bool __test_item(void *ptr)
{
    item *pi = (item *)ptr;
    return (strncmp(pi->name, *__item_name, MAX_STRING_SIZE) == 0);
}


static inline void command_error_arg(int cmd)
{
    printf("Error using %s(%d): number of arguments error\n", g_command_str[cmd], g_argument_size[cmd]);
}

void infosystem_run(infosystem *isptr)
{
    char *line = NULL;

    size_t size;
    // 从终端读入一行命令并解析
    // 如<cmd> <arg1> <arg2> <arg3> ...
    
    while (printf("> "), getline(&line, &size, stdin) != -1)
    {
        // 只输入一个回车时，什么也不做
        if (strncmp(line, "\n", 1) == 0)
            continue;
        
        // 将读入的字符串拷贝并以空格分割
        char *data = (char *)malloc((size + 1) * sizeof(char));
        strncpy(data, line, size);

        char *pch = strtok(data, " \n");

        // 寻找命令代码
        enum ECmd i;
        for (i = 0; i < N_COMMAND; i++)
        {
            if (strcmp((pch), g_command_str[i]) == 0)
                break;
        }
        if (i == N_COMMAND) // 无效命令
        {
            printf("Invalid command: %s\n", pch);
            puts("Type 'help↵' for help.");
        }
        else // 命令有效，读入参数
        {
            if (g_argument_size[i] == 0) // 命令无参数
            {
                if ((pch = strtok(NULL, " \n")) != NULL)
                    command_error_arg(i);
                else
                    infosystem_dispatch(isptr, i, NULL);
            }
            else // 命令有参数
            {
                int j = 0;
                char **argv = (char **) malloc(g_argument_size[i] * sizeof(char *));
                char *tmp;

                while ((tmp = strtok(NULL, " \n")) != NULL && j != g_argument_size[i])
                {
                    argv[j] = tmp;
                    j++;
                }


                // 如果参数个数正确则准备执行命令，否则提示出错
                if (j == g_argument_size[i] && tmp == NULL)
                    infosystem_dispatch(isptr, i, argv);
                else
                    command_error_arg(i);
                free(argv);
            }
        }

        free(data);
    }
}

FILE *__sales_stock_fp = NULL;
void __brand_sales_stock_output(void *ptr)
{
    brand_sales_stock_output((brand *)ptr, __sales_stock_fp);
}

void __save_brand_sales_stock(void *ptr)
{
    item *iptr = (item *)ptr;
    linklist_iterate(iptr->brand_list, __brand_sales_stock_output);
}

void infosystem_release(infosystem *isptr)
{
    FILE *fp = fopen("config/item.txt", "w");
    if (fp)
    {
        item_traverse_tree(isptr->base_item, fp);
        fclose(fp);
    }
    else
    {
        puts("Error release: cannot open config/item.txt!");
        exit(-1);
    }
    
    __sales_stock_fp = fopen("config/sales.txt", "w");
    assert(__sales_stock_fp);
    linklist_iterate(isptr->item_list, __save_brand_sales_stock);
    fclose(__sales_stock_fp);
    
    linklist_iterate(isptr->item_list, __save_item_brand);
    linklist_destroy(isptr->item_list);
    
    item_destroy_tree(isptr->base_item);
}
