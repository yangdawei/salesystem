//
//  brand.c
//  SaleSystem
//
//  Created by Dawei Yang on 5/28/15.
//  Copyright (c) 2015 Dawei Yang. All rights reserved.
//

#include <string.h>
#include <assert.h>

#include "brand.h"

brand *brand_create()
{
    brand *ptr = (brand *)malloc(sizeof(brand));
    ptr->belong = NULL;
    ptr->value_len = 0;
    ptr->sales = 0;
    ptr->stock = 0;
    return ptr;
}

void brand_destroy(brand *ptr)
{
    free(ptr);
}

void brand_sales_stock_output(brand *ptr, FILE *fp)
{
    fprintf(fp, "%s %d %d\n", ptr->name, ptr->sales, ptr->stock);
}

void brand_output(brand *ptr, FILE *fp, bool show_sales_stock)
{
    if (!show_sales_stock)
        fprintf(fp, "[%s]\n", ptr->name);
    else
        fprintf(fp, "[%s] (%d %d)\n", ptr->name, ptr->sales, ptr->stock);
    const char **names_pos = malloc(ptr->value_len * sizeof(const char *));
    const mytype **types_pos = malloc(ptr->value_len * sizeof(mytype *));
    
    ssize_t count = ptr->value_len;
    for (item *pi = ptr->belong; pi != NULL; pi = pi->parent)
    {
        count -= pi->props_len;
        for (size_t i = 0; i < pi->props_len; i++)
        {
            names_pos[i + count] = pi->props_name[i];
            types_pos[i + count] = pi->props_type + i;
        }
        
    }
    assert(count == 0);
    
    size_t max_len = 0;
    for (size_t i = 0; i < ptr->value_len; i++)
        max_len = max_len > strlen(names_pos[i]) ? max_len : strlen(names_pos[i]);
    
    max_len += 1;
    for (size_t i = 0; i < ptr->value_len; i++)
    {
        fprintf(fp, "%s%*c : ",
                names_pos[i],
                (int)(max_len - strlen(names_pos[i])), ' ');
    
        switch (*types_pos[i])
        {
            case INTEGER:
                fprintf(fp, "%d\n", ptr->value[i].i);
                break;
                
            case FLOAT:
                fprintf(fp, "%f\n", ptr->value[i].f);
                break;
                
            case STRING_128:
                fprintf(fp, "%s\n", ptr->value[i].s);
                break;
            
            default:
                assert(false);
        }
    }
    
    fprintf(fp, "\n");
    
    free(names_pos);
    free(types_pos);
}

void brand_save_list(linklist *brand_list, FILE *fp)
{
    for (linknode *ptr = brand_list->head; ptr != brand_list->tail; ptr = ptr->next)
        brand_output((brand *)ptr->data_ptr, fp, false);
}

void brand_load_list(item *belong, linklist *brand_list, FILE *fp)
{
    char *line = NULL;
    size_t len = 0;
    ssize_t read = 0;
    size_t nprops = 0;
    
    for (item *ptr = belong; ptr != NULL; ptr = ptr->parent)
        nprops += ptr->props_len;
    
    while ((read = getline(&line, &len, fp)) != -1)
    {
        if (len == 0)
            continue;
        if (strncmp(line, "\n", 1) == 0)
            continue;
        if (strncmp(line, "--\n", 3) == 0)
            break;
        if (line[0] == '[')
        {
            char *name_end = strchr(line, ']');
            if (name_end == NULL)
                exit(-5);
            *name_end = '\0';
            
            brand *pb = brand_create();
            pb->belong = belong;
            pb->value_len = nprops;
            strncpy(pb->name, line + 1, MAX_STRING_SIZE);
            linklist_add(brand_list, pb);
        }
        else
        {
            assert(!linklist_isempty(brand_list));
                
            brand *pb = (brand *)brand_list->tail->prev->data_ptr;
            assert(pb);
            
            char buf_key[129], buf_value[129];
            int result = sscanf(line, "%128s : %128s", buf_key, buf_value);
            if (result != 2)
            {
                printf("Warning: cannot parse line '%s', ignored.\n", line);
                continue;
            }
            
            mytype type;
            ssize_t pos = item_find_prop_pos(belong, buf_key, &type);
            if (pos < 0)
            {
                printf("Warning: cannot find property '%s'.\n", buf_key);
                continue;
            }
            switch (type)
            {
                case INTEGER:
                    sscanf(buf_value, "%d", &pb->value[pos].i);
                    break;
                    
                case FLOAT:
                    sscanf(buf_value, "%f", &pb->value[pos].f);
                    break;
                    
                case STRING_128:
                    sscanf(buf_value, "%128s", pb->value[pos].s);
                    break;
                    
                default:
                    assert(false);
            }
        }
    }
}
