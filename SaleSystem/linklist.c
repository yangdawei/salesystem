#include "linklist.h"

linknode *linknode_create()
{
    linknode *ptr = (linknode *)malloc(sizeof(linknode));
    ptr->next = NULL;
    ptr->prev = NULL;
    ptr->data_ptr = NULL;
    return ptr;
}
void linknode_destroy(linknode *ptr)
{
    free(ptr);
}

linklist *linklist_create()
{
    linklist *ptr = (linklist *)malloc(sizeof(linklist));
    ptr->head = ptr->tail = linknode_create();
    
    return ptr;
}

void linklist_delete(linklist *ptr, linknode *del_ptr)
{
    linknode *pos;
    for (pos = ptr->head; pos != ptr->tail; pos = pos->next)
    {
        if (pos == del_ptr)
            break;
    }
    if (pos == ptr->tail)
    {
        printf("Warning: delete node failed.\n");
        return;
    }
    
    if (pos == ptr->head)
    {
        ptr->head = ptr->head->next;
        linknode_destroy(pos);
    }
    else
    {
        linknode *prev_node = pos->prev;
        linknode *next_node = pos->next;
        prev_node->next = next_node;
        next_node->prev = prev_node;
        linknode_destroy(pos);
    }
}

void linklist_destroy(linklist *ptr)
{
    while (ptr->head != ptr->tail)
        linklist_delete(ptr, ptr->head);
    linknode_destroy(ptr->head);
    free(ptr);
}

void linklist_add(linklist *ptr, void *data_ptr)
{
    ptr->tail->data_ptr = data_ptr;
    linknode *tail_new = linknode_create();
    tail_new->prev = ptr->tail;
    ptr->tail->next = tail_new;
    ptr->tail = tail_new;
}

void linklist_iterate(linklist *ptr, void (*func)(void *))
{
    for (linknode *pos = ptr->head; pos != ptr->tail; pos = pos->next)
        func(pos->data_ptr);
}

void linklist_iterate_with_test(linklist *ptr, bool (*test)(void *), void (*func)(void *))
{
    for (linknode *pos = ptr->head; pos != ptr->tail; pos = pos->next)
        if (test(pos->data_ptr))
            func(pos->data_ptr);
}

bool linklist_isempty(linklist *ptr)
{
    return ptr->head == ptr->tail;
}

void *linklist_find(linklist *ptr, bool (*func)(void *))
{
    for (linknode *it = ptr->head; it != ptr->tail; it = it->next)
    {
        if (func(it->data_ptr))
            return it->data_ptr;
    }
    return NULL;
}

void linklist_concat(linklist *p1, linklist **p2)
{
    p1->tail->next = (*p2)->head;
    (*p2)->head->prev = p1->tail;
    linknode *tmp = p1->tail;
    p1->tail = (*p2)->tail;
    linklist_delete(p1, tmp);
    (*p2) = linklist_create();
}

void linklist_sort(linklist *ptr, int (*compare)(void *left, void *right))
{
    for (linknode *p1 = ptr->head; p1 != ptr->tail; p1 = p1->next)
    {
        for (linknode *p2 = ptr->tail->prev; p2 != p1; p2 = p2->prev)
        {
            if (compare(p2->prev->data_ptr, p2->data_ptr) < 0)
            {
                void *tmp = p2->prev->data_ptr;
                p2->prev->data_ptr = p2->data_ptr;
                p2->data_ptr = tmp;
            }
        }
    }
}
