#ifndef BRAND_H
#define BRAND_H

#include <stdlib.h>

#include "item.h"
#include "linklist.h"

typedef union _vartype
{
    int i;
    float f;
    char s[129];
} vartype;

typedef struct _brand
{
    char name[MAX_STRING_SIZE+1];
    item *belong;
    vartype value[MAX_PROP_SIZE];
    size_t value_len;
    int sales;
    int stock;
} brand;

brand *brand_create();

void brand_output(brand *ptr, FILE *fp, bool show_sales_stock);
void brand_destroy(brand *ptr);
void brand_load_list(item *belong, linklist *brand_list, FILE *fp);
void brand_save_list(linklist *brand_list, FILE *fp);
void brand_sales_stock_output(brand *ptr, FILE *fp);

#endif
