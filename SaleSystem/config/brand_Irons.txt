[Philips_GC1480/02]
Price_CNY          : 199.000000
Power_W            : 1200
Capacity_mL        : 270
SteamAmount_g/min  : 17
Weight_kg          : 0.889000

[Philips_GC525]
Price_CNY          : 630.000000
Power_W            : 1500
Capacity_mL        : 1000
SteamAmount_g/min  : 35
Weight_kg          : 4.110000

