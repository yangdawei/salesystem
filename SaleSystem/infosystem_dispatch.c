#include <string.h>
#include <assert.h>

#include "infosystem.h"
#include "linklist.h"
#include "brand.h"

extern const char *g_command_str[];
extern const char *g_command_help[];
extern const int g_argument_size[];
extern const char **__item_name;

extern bool __test_item(void *ptr);
extern void brand_save_list(linklist *brand_list, FILE *fp);
extern void brand_load_list(item *belong, linklist *brand_list, FILE *fp);

ssize_t __brand_prop_pos;
mytype __brand_prop_type;
vartype __brand_prop_val;


bool __query_brand_by_property(void *ptr)
{
    brand *pb = (brand *)ptr;
    if (__brand_prop_type == INTEGER)
        return pb->value[__brand_prop_pos].i == __brand_prop_val.i;
    if (__brand_prop_type == FLOAT)
        return pb->value[__brand_prop_pos].f == __brand_prop_val.f;
    if (__brand_prop_type == STRING_128)
        return (strncmp(pb->value[__brand_prop_pos].s, __brand_prop_val.s, MAX_STRING_SIZE) == 0);
    return false;
}

void __print_brand_with_sales(void *ptr)
{
    brand_output((brand *)ptr, stdout, true);
}


char *__brand_name = NULL;
bool __find_brand_by_name(void *ptr)
{
    return (strncmp(__brand_name, ((brand *)ptr)->name, MAX_STRING_SIZE) == 0);
}

int __compare_brand_by_property(void *left, void *right)
{
    brand *pl = (brand *)left;
    brand *pr = (brand *)right;
    if (__brand_prop_type == INTEGER)
        return pl->value[__brand_prop_pos].i - pr->value[__brand_prop_pos].i;
    else if (__brand_prop_type == FLOAT)
        return pl->value[__brand_prop_pos].f - pr->value[__brand_prop_pos].f;
    else if (__brand_prop_type == STRING_128)
        return strncmp(pl->value[__brand_prop_pos].s, pl->value[__brand_prop_pos].s, MAX_STRING_SIZE);
    return 0;
}

int __compare_brand_by_sales(void *left, void *right)
{
    brand *pl = (brand *)left;
    brand *pr = (brand *)right;
    return pl->sales - pr->sales;
}

float __total_brand_sales;
void __add_brand_sales(void *ptr)
{
    brand *pb = (brand *)ptr;
    __total_brand_sales += pb->sales * pb->value[__brand_prop_pos].f;
}

void infosystem_dispatch(infosystem *isptr, int cmd, char **argv)
{
    switch(cmd)
    {
        case HELP:
        {
            int i;
            for (i = 0; i < N_COMMAND; i++)
                printf("%s(%d): %s\n", g_command_str[i], g_argument_size[i], g_command_help[i]);
            break;
        }
            
        case LIST_ITEM:
            item_traverse_tree(isptr->base_item, stdout);
            break;
        case LOAD_ITEM:
        {
            FILE *fp = fopen(argv[0], "r");
            if (fp == NULL)
            {
                printf("Cannot open file %s.\n", argv[0]);
                return;
            }
            else
            {
                item *ptr = item_load_tree(fp);
                fclose(fp);
                item_destroy_tree(isptr->base_item);
                isptr->base_item = ptr;
            }
            break;
        }
        case SAVE_ITEM:
        {
            FILE *fp = fopen(argv[0], "w");
            if (!fp)
            {
                printf("Error open file %s, command execution failed.\n", argv[0]);
                return;
            }
            item_traverse_tree(isptr->base_item, fp);
            fclose(fp);
            break;
        }
        case LIST_BRAND:
        {
            __item_name = (const char **)argv;
            item *ptr = (item *)linklist_find(isptr->item_list, __test_item);
            if (!ptr)
                printf("Error: cannot find item named '%s'\n", argv[0]);
            else
                brand_save_list(ptr->brand_list, stdout);
            break;
        }
        case LOAD_BRAND:
        {
            __item_name = (const char **)argv;
            item *ptr = (item *)linklist_find(isptr->item_list, __test_item);
            if (!ptr)
                printf("Error: cannot find item named '%s'\n", argv[0]);
            else
            {
                FILE *fp = fopen(argv[1], "r");
                if (!fp)
                    printf("Error: cannot open file '%s'\n", argv[1]);
                else
                    brand_load_list(ptr, ptr->brand_list, fp);
            }
            break;
        }
        case SAVE_BRAND:
        {
            __item_name = (const char **)argv;
            item *ptr = (item *)linklist_find(isptr->item_list, __test_item);
            if (!ptr)
                printf("Error: cannot find item named '%s'\n", argv[0]);
            else
            {
                FILE *fp = fopen(argv[1], "w");
                if (!fp)
                    printf("Error: cannot open file '%s'\n", argv[1]);
                else
                {
                    brand_save_list(ptr->brand_list, fp);
                    fclose(fp);
                }
            }
            break;
        }
        case ADD_BRAND:
        {
            __item_name = (const char **)argv;
            item *ptr = (item *)linklist_find(isptr->item_list, __test_item);
            if (!ptr)
                printf("Error: cannot find item named '%s'\n", argv[0]);
            else
            {
                linklist *tmp = linklist_create();
                brand_load_list(ptr, tmp, stdin);
                linklist_concat(ptr->brand_list, &tmp);
                linklist_destroy(tmp);
            }
            break;
        }
        case QUERY:
        {
            const char *buf_key = strtok(argv[0], ":");
            const char *buf_val = strtok(NULL, ":");
            
            
            if (!buf_val)
                printf("Error parsing '%s'.\n", argv[0]);
            else
            {
                for (linknode *p1 = isptr->item_list->head; p1 != isptr->item_list->tail; p1 = p1->next)
                {
                    item *pi = (item *)p1->data_ptr;
                    __brand_prop_pos = item_find_prop_pos(pi, buf_key, &__brand_prop_type);
                    
                    if (__brand_prop_type == INTEGER)
                        sscanf(buf_val, "%d", &__brand_prop_val.i);
                    if (__brand_prop_type == FLOAT)
                        sscanf(buf_val, "%f", &__brand_prop_val.f);
                    if (__brand_prop_type == STRING_128)
                        sscanf(buf_val, "%128s", __brand_prop_val.s);
                    
                    if (__brand_prop_pos > 0)
                        linklist_iterate_with_test(pi->brand_list, __query_brand_by_property, __print_brand_with_sales);
                }
            }
            break;
        }
            
        case SELL:
        {
            brand *pb = NULL;
            for (linknode *p1 = isptr->item_list->head; p1 != isptr->item_list->tail; p1 = p1->next)
            {
                item *pi = (item *)p1->data_ptr;
                __brand_name = argv[0];
                pb = (brand *)linklist_find(pi->brand_list, __find_brand_by_name);
                if (pb)
                    break;
            }
            if (!pb)
                printf("Cannot find brand named '%s'\n", argv[0]);
            else
            {
                if (pb->stock == 0)
                    printf("Cannot sell '%s': out of stock.\n", argv[0]);
                else
                {
                    printf("Sell '%s' succeeded.\n", argv[0]);
                    pb->sales++;
                    pb->stock--;
                }
                if (pb->stock <= 3 && pb->stock > 0)
                    printf("Attention: Only %d of '%s', please purchase ASAP.\n", pb->stock, argv[0]);
                else if (pb->stock == 0)
                    printf("Attention: '%s' is out of stock, please purchase ASAP.\n", argv[0]);
            }
            break;
        }
        case PURCHASE:
        {
            brand *pb = NULL;
            for (linknode *p1 = isptr->item_list->head; p1 != isptr->item_list->tail; p1 = p1->next)
            {
                item *pi = (item *)p1->data_ptr;
                __brand_name = argv[0];
                pb = (brand *)linklist_find(pi->brand_list, __find_brand_by_name);
                if (pb)
                    break;
            }
            if (!pb)
                printf("Cannot find brand named '%s'\n", argv[0]);
            else
            {
                int n_purchase = 0;
                sscanf(argv[1], "%d\n", &n_purchase);
                pb->stock += n_purchase;
                printf("Purchase succeeded. Now %d of %s are in stock.\n", pb->stock, pb->name);
            }
            break;
        }
        case SORT_BRAND:
        {
            __item_name = (const char **)argv;
            item *ptr = (item *)linklist_find(isptr->item_list, __test_item);
            if (!ptr)
                printf("Error: cannot find item named '%s'\n", argv[0]);
            else
            {
                __brand_prop_pos = item_find_prop_pos(ptr, argv[1], &__brand_prop_type);
                linklist_sort(ptr->brand_list, __compare_brand_by_property);
                linklist_iterate(ptr->brand_list, __print_brand_with_sales);
            }
            
            break;
        }
        case SORT_SALES:
        {
            linklist *all_brands = linklist_create();
            for (linknode *p1 = isptr->item_list->head; p1 != isptr->item_list->tail; p1 = p1->next)
            {
                item *pi = (item *)p1->data_ptr;
                for (linknode *p2 = pi->brand_list->head; p2 != pi->brand_list->tail; p2 = p2->next)
                    linklist_add(all_brands, p2->data_ptr);
            }
            
            linklist_sort(all_brands, __compare_brand_by_sales);
            linklist_iterate(all_brands, __print_brand_with_sales);
            
            linklist_destroy(all_brands);
            break;
        }
        case SUM_SALES:
        {
            __item_name = (const char **)argv;
            item *ptr = (item *)linklist_find(isptr->item_list, __test_item);
            if (!ptr)
                printf("Error: cannot find item named '%s'\n", argv[0]);
            else
            {
                __total_brand_sales = 0;
                __brand_prop_pos = item_find_prop_pos(ptr, "Price_CNY", NULL);
                assert(__brand_prop_pos >= 0);
                linklist_iterate(ptr->brand_list, __add_brand_sales);
                printf("Total sales of '%s': %.2f CNY\n", ptr->name, __total_brand_sales);
            }
            break;
        }
            
        default:
            printf("%s(%d): Unimplemented\n", g_command_str[cmd], g_argument_size[cmd]);
    }
    
}
