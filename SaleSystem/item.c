#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#include "item.h"
#include "brand.h"

const char *g_typename[] = 
{
    "Integer",
    "Float",
    "String_128"
};

item *item_create()
{
    item *ptr = (item *)malloc(sizeof(item));
    ptr->len = 0;
    ptr->props_len = 0;
    
    ptr->brand_list = linklist_create();

    return ptr;
}

void adapt_brand_destroy(void *ptr)
{
    brand_destroy((brand *) ptr);
}

void item_destroy(item *ptr)
{
    linklist_iterate(ptr->brand_list, adapt_brand_destroy);
    linklist_destroy(ptr->brand_list);
    free(ptr);
}

void item_output(item *ptr, FILE *fp)
{
    if (ptr->parent == NULL)
        fprintf(fp, "[%s]\n", ptr->name);
    else
        fprintf(fp, "[%s] : [%s]\n", ptr->name, ptr->parent->name);

    size_t i;
    size_t max_len = 0;
    for (i = 0; i < ptr->props_len; i++)
        max_len = max_len > strlen(ptr->props_name[i]) ? max_len : strlen(ptr->props_name[i]);
    max_len += 1;
    for (i = 0; i < ptr->props_len; i++)
        fprintf(fp, "%s%*c : %s\n",
                ptr->props_name[i],
                (int)(max_len - strlen(ptr->props_name[i])),
                ' ',
                g_typename[ptr->props_type[i]]);
    fprintf(fp, "\n");
}

static inline void item_add_child(item *parent, item *child)
{
    if (parent->len == MAX_CHILDREN_SIZE)
    {
        printf("Error add child %s <- %s: children size exceeds limit!", child->name, parent->name);
        exit(-4);
    }
    parent->children[parent->len] = child;
    parent->len++;
    child->parent = parent;
}

static inline void item_add_prop(item *ptr, const char *name, const char *type)
{
    if (ptr->props_len == MAX_PROP_SIZE)
    {
        printf("Error: property length exceeds max length while adding %s : %s\n", name, type);
        exit(-3);
    }
    mytype i;
    for (i = 0; i < N_TYPE; i++)
        if (strncmp(g_typename[i], type, MAX_STRING_SIZE) == 0)
            break;
    if (i == N_TYPE) // 未找到
    {
        printf("Error while adding %s : %s: cannot find type '%s'", name, type, type);
        exit(-3);
    }
    else
    {
        strncpy(ptr->props_name[ptr->props_len], name, MAX_STRING_SIZE);
        ptr->props_type[ptr->props_len] = i;
        ptr->props_len++;
    }
}

void item_traverse_tree(item *ptr, FILE *fp)
{
    item_output(ptr, fp);
    size_t i;

    for (i = 0; i < ptr->len; i++)
        item_traverse_tree(ptr->children[i], fp);
}

item *item_load_tree(FILE *fp)
{
    item *ptr = item_create();

    item *item_buffer[256];
    size_t item_buffer_len = 0;

    char *line = NULL;
    size_t len = 0;
    ssize_t read = 0;

    while ((read = getline(&line, &len, fp)) != -1)
    {
        printf("--------%s", line);
        if (len == 0)
            continue;
        if (strncmp(line, "\n", 1) == 0)
            continue;
        if (strncmp(line, "--\n", 3) == 0)
            break;
        if (line[0] == '[')
        {
            if (strchr(line, ':') == NULL) // Base item
            {
                char *p2 = strchr(line, ']');
                ssize_t len = p2 - line - 1;
                if (len > MAX_STRING_SIZE)
                    len = MAX_STRING_SIZE;
                strncpy(ptr->name, line + 1, len);
                printf(">>> Base item name: %s\n", ptr->name);
                item_buffer[item_buffer_len] = ptr;
                item_buffer_len++;
            }
            else
            {
                char *p2 = strchr(line, ']');
                char *derive_name = line + 1;
                char *p3 = strchr(derive_name, '[');
                char *base_name = p3 + 1;
                char *p4 = strchr(base_name, ']');
                *p2 = '\0';
                *p4 = '\0';


                size_t i;
                int find = 0;
                for (i = 0; i < item_buffer_len; i++)
                {
                    if (strncmp(item_buffer[i]->name, base_name, MAX_STRING_SIZE) == 0)
                    {
                        item *chd = item_create();
                        strncpy(chd->name, derive_name, MAX_STRING_SIZE);
                        
                        item_add_child(item_buffer[i], chd);

                        item_buffer[item_buffer_len] = chd;
                        item_buffer_len++;
                        find = 1;
                        printf(">>> %s -> %s proccessed.\n", chd->parent->name, chd->name);
                    }
                }
                if (!find)
                {
                    printf("Unable to find parent item '%s' of '%s'\n", base_name, derive_name);
                    free(ptr);
                    return NULL;
                }
            }
        }
        else
        {
            char buf_key[MAX_STRING_SIZE+1], buf_type[MAX_STRING_SIZE+1];
            int result = sscanf(line, " %128s : %128s ", buf_key, buf_type);
            assert(result == 2);
            item *ptr = item_buffer[item_buffer_len-1];
            item_add_prop(ptr, buf_key, buf_type);
        }
    }

    return ptr;
}

void item_destroy_tree(item *root)
{
    size_t i;
    // printf("Start destroy. %s has %ld child(ren).\n", root->name, root->len);
    for (i = 0; i < root->len; i++)
        item_destroy_tree(root->children[i]);

    // printf("Destroy item node '%s'.\n", root->name);
    item_destroy(root);
}

ssize_t item_find_prop_pos(item *belong, const char *prop_name, mytype *type)
{
    size_t nprops = 0;
    for (item *ptr = belong; ptr != NULL; ptr = ptr->parent)
        nprops += ptr->props_len;
    
    ssize_t count = 0;
    size_t i = 0;
    item *ptr;
    bool find = false;
    for (ptr = belong; ptr != NULL; ptr = ptr->parent)
    {
        count += ptr->props_len;
        for (i = 0; i < ptr->props_len; i++)
        {
            if (strncmp(ptr->props_name[i], prop_name, MAX_STRING_SIZE) == 0)
            {
                find = true;
                break;
            }
        }
        if (find)
            break;
    }
    if (!find)
    {
        return -1;
    }
    if (type != NULL)
        *type = ptr->props_type[i];
    return nprops - count + i;
}
