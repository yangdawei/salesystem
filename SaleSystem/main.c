#include "infosystem.h"

int main(int argc, char *argv[])
{
    infosystem is;
    
    infosystem_init(&is);
    infosystem_run(&is);
    infosystem_release(&is);

    return 0;
}
