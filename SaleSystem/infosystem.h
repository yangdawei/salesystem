#ifndef INFOSYSTEM_H
#define INFOSYSTEM_H

#define MAX_BRAND_SIZE 128

#include <stdio.h>

#include "item.h"
#include "linklist.h"

/**
 * 销售系统结构，最顶入口数据
 * 条目(item)与品牌(brand)的关系详见item.h与brand.h
 */
typedef struct _infosystem
{
    // 根条目，所有的条目依继承关系组成一个树
    item *base_item;
    
    // 为方便访问，把所有的条目同时也组成一个链表
    linklist *item_list;
    
    // 所有的品牌数量
    size_t brand_len;
    
    // 所有品牌的名称、销售量、库存量的数组
    char brand_names[MAX_STRING_SIZE][MAX_BRAND_SIZE];
    int brand_sales[MAX_BRAND_SIZE];
    int brand_stock[MAX_BRAND_SIZE];
} infosystem;

/*
 * 销售系统各个命令的代码
 */
enum ECmd
{
    HELP,       // 显示帮助
    LIST_ITEM,  // 显示所有的条目
    LOAD_ITEM,  // 从文件载入条目
    SAVE_ITEM,  // 将条目都存储到一个文件
    LIST_BRAND, // 列出某条目对应的所有品牌
    LOAD_BRAND, // 从一个文件载入某条目的所有品牌信息
    SAVE_BRAND, // 将某条目的所有品牌信息存储到文件
    ADD_BRAND,  // 从命令行手动输入信息给一个条目添加一个品牌
    QUERY,      // 根据给定的一个属性与值搜索所有符合条件的条目
    SELL,       // 模拟买出一件某品牌的商品
    PURCHASE,   // 模拟购入指定数量的某品牌的商品
    SORT_BRAND, // 对某条目的所有商品按给定属性排序并输出
    SORT_SALES, // 对所有商品按销量排序输出
    SUM_SALES,  // 计算某一条目的销量总额
    N_COMMAND
};

void infosystem_init(infosystem *isptr);
void infosystem_run(infosystem *isptr);
void infosystem_release(infosystem *isptr);

#endif