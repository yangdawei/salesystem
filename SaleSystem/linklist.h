#ifndef LINKLIST_H
#define LINKLIST_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct _linknode
{
    struct _linknode *prev;
    struct _linknode *next;
    void *data_ptr;
} linknode;

typedef struct _linklist
{
    linknode *head;
    linknode *tail;
} linklist;

linknode *linknode_create();
void linknode_destroy(linknode *ptr);

linklist *linklist_create();
void linklist_destroy(linklist *ptr);

void linklist_add(linklist *ptr, void *data_ptr);
void linklist_delete(linklist *ptr, linknode *del_ptr);
void linklist_destroy(linklist *ptr);
void linklist_iterate(linklist *ptr, void (*func)(void *));
void linklist_iterate_with_test(linklist *ptr, bool (*test)(void *), void (*func)(void *));
bool linklist_isempty(linklist *ptr);
void linklist_concat(linklist *p1, linklist **p2);
void *linklist_find(linklist *ptr, bool (*func)(void *));
void linklist_sort(linklist *ptr, int (*compare)(void *left, void *right));

#endif
