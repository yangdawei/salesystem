#ifndef ITEM_H
#define ITEM_H

#define MAX_PROP_SIZE 128
#define MAX_CHILDREN_SIZE 16
#define MAX_STRING_SIZE 128

#include "linklist.h"

typedef enum
{
    INTEGER, FLOAT, STRING_128, N_TYPE
} mytype;

extern const char *g_typename[];

typedef struct _item
{
    char name[MAX_STRING_SIZE + 1];
    char props_name[MAX_PROP_SIZE][MAX_STRING_SIZE + 1];
    mytype props_type[MAX_PROP_SIZE];
    size_t props_len;
    
    linklist *brand_list;

    struct _item *parent;
    struct _item *children[MAX_CHILDREN_SIZE];
    size_t len;
} item;

item *item_load_tree(FILE *fp);
item *item_create();

// void item_add_prop(item *ptr, const char *name, const char *type);
void item_output(item *ptr, FILE *fp);
void item_destroy(item *ptr);
void item_destroy_tree(item *root);
void item_traverse_tree(item *root, FILE *fp);
ssize_t item_find_prop_pos(item *belong, const char *prop_name, mytype *type);

#endif
